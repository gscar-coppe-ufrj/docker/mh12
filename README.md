# Description
Ubuntu-based image file used to compile and run stuff to control Motoman MH12 robot using ROS. It is recommended to pull the Docker image `gscar/motoman-mh12`, but one may also build running the provided build script.

# Pull from Repository
```
docker pull gscar/motoman-mh12
```

# Build
It is recommended to pull the image using the previous steps. Alternatively, if you wish to build the image yourself, please use the provided build script.
```
$ git clone https://gitlab.com/gscar-coppe-ufrj/docker/mh12 gscar-mh12
$ cd gscar-mh12
$ chmod +x build
$ ./build
```
This script will generate the Docker image and name it as `gscar/motoman-mh12`.

# Run
To run the container, we recommend using the provided `docker-mh12` script. For that, give run permission through `chmod +x docker-mh12` and place the script somewhere in your path.
```
$ docker-mh12 [your command goes here]
```
The script mounts the current directory on the container's `/data` directory. If you run the script from you catkin workspace directory, it will automatically source `devel/setup.bash`.

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
